#!/bin/bash
set -e

if [ -z "$AZP_URL" ]; then
  echo 1>&2 "error: missing AZP_URL environment variable"
  exit 1
fi

if [ -z "$AZP_TOKEN" ]; then
  echo 1>&2 "error: missing AZP_TOKEN environment variable"
  exit 1
fi

if [ -z "$AZP_AGENT_NAME" ]; then
  echo 1>&2 "error: missing AZP_AGENT_NAME environment variable"
  exit 1
fi

if [ -z "$AZP_POOL" ]; then
  echo 1>&2 "error: missing AZP_POOL environment variable"
  exit 1
fi

print_header() {
  lightcyan='\033[1;36m'
  nocolor='\033[0m'
  echo -e "${lightcyan}$1${nocolor}"
}

print_header "1. check environment ..."

export AGENT_ALLOW_RUNASROOT="yes"
echo "AZP_AGENT_NAME: ${AZP_AGENT_NAME}"
echo "AZP_URL: ${AZP_URL}"
echo "AZP_TOKEN: ${AZP_TOKEN}"
echo "AZP_POOL: ${AZP_POOL}"

print_header "2. Configuring Azure Pipelines agent..."

./config.sh --unattended \
  --url "${AZP_URL}" \
  --auth pat \
  --token "${AZP_TOKEN}" \
  --pool "${AZP_POOL}" \
  --agent "${AZP_AGENT_NAME}" \
  --replace \
  --acceptTeeEula

print_header "3. Start Docker Daemon..."

dockerd &

print_header "4. start docker daemon..."

./run.sh
